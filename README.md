# rhel-lab
Test lab setup for preparing for the Red Hat RHCSA/RHCE exams. Inspired by the VM's provided with the book [Red Hat RHCE/RHCSA 7 Cert Guide][1] written by Sander van Vugt  and the real deal on the actual exam.

All credits go to the original author: [allyourco_de](https://bitbucket.org/allyourco_de/rhel-lab.git). I made a few alterations:
- Switched to CentOS 7.0 to better reflect the exam environment.
- Did a kernel upgrade to fix targetcli saveconfig fail.
- Set SELinux in enforcing mode on server and desktop machine.
- Changed nameserver order in /etc/resolv.conf so classroom.example.com is queried first.
- Added public ssh key (id_rsa.pub) under scripts/ for key-based ssh authentication.
- Added extra network adapters for practising with network teaming.
- Added an (empty) optical drive for setting up a local yum repo.

## Description
This will create 3 VM's with, besides the vagrant user, the following users:

User            | Password
----------------|--------
student (wheel) | student
root            | redhat

#### classroom.example.com
* ip: 172.16.0.16.
* ipa server: https://classroom.example.com
* cert & keytabs: ftp://classroom.example.com
    - server.keytab is configured for nfs and cifs
* ipa administartor admin with password 'password'
* ipa user lisa with password 'password'
* ipa user linda with password 'password'

#### server.example.com
* ip: 172.16.0.11
* minimal server
* with an extra 10 GiB unpartitioned drive
* added as an ipa-client to the realm EXAMPLE.COM

#### desktop.example.com
* ip: 172.16.0.10
* with an extra 10 GiB unpartitioned drive
* Server with GUI (installed but not enabled)

## Requirements
* Vagrant >= 2.0.1
* libvirt or VirtualBox
* atleast 4 GiB free memory (with recommended settings)
* atleast 20 GiB free storage  (with recommended settings)

## Install

To be able to access the ipa interface at `https://classroom.example.com` you will need to modify your hosts file:
```
echo '172.16.0.16. classroom.example.com classroom' >> /etc/hosts
```
Or else access it via the GUI on the VM `desktop.example.com`

Edit the `Vagrantfile` and modify `vbox_vm_path` to match your VirtualBox VMs directory.<br>
Optional:
- place your own public ssh key in ./scripts/: `cp -fv ~/.ssh/id_rsa.pub ./rhce-lab/scripts/`.
- setup your local ~/.ssh/config file:

```
Host classroom.example.com
  User root
  HostName 172.16.0.254
  StrictHostKeychecking no
  UserKnownHostsFile /dev/null

Host desktop.example.com
  User root
  HostName 172.16.0.10
  StrictHostKeychecking no
  UserKnownHostsFile /dev/null

Host server.example.com
  User root
  HostName 172.16.0.11
  StrictHostKeychecking no
  UserKnownHostsFile /dev/null
```

- Now you can `ssh server.example.com` after `vagrant up`.

Also the following environment variables can be set instead of editing the `Vagrantfile`:
`SUBSCRIPTION_USERNAME`, `SUBSCRIPTION_PASSWORD`, `VBOX_VM_PATH`, `LIBVIRT_STORAGE_POOL`

### CentOS version:

```
cd rhel-lab
vagrant up
```

### RHEL version:
_Note: The rhel version requires an active subscription._

Download both the **RHEL 7.3 Vagrant box for libvirt or VirtualBox** and the **Red Hat Container Tools** from [access.redhat.com][2].
```
unzip cdk-*.zip && cd cdk/plugins
vagrant plugin install vagrant-registration
vagrant box add rhel/7.3 file://rhel-cdk-kubernetes-*.vagrant-*.box
export SUBSCRIPTION_USERNAME='foo' SUBSCRIPTION_PASSWORD='bar'
cd rhel-lab
vagrant up
```

[1]: http://www.sandervanvugt.com/books/ "Red Hat RHCE/RHCSA 7 Cert Guide"
[2]: https://access.redhat.com/downloads/content/293/ver=2.4/rhel---7/2.4.0/x86_64/product-software "access.redhat.com"

---

